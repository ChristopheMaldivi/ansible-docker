#!/bin/sh

DOCKER_IMAGE_NAME="ansible-docker"

CMD="$*"
PWD=$(pwd)
echo run: docker run -v "${PWD}":"${PWD}" -w "${PWD}" "$DOCKER_IMAGE_NAME" bash -c "${CMD}"
docker run -v "${PWD}":"${PWD}" -w "${PWD}" "$DOCKER_IMAGE_NAME" bash -c "${CMD}"
