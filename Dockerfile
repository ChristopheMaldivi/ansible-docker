FROM alpine:3.13

LABEL maintainer="christophe.maldivi@orange.com"

ENV CRYPTOGRAPHY_DONT_BUILD_RUST 1

RUN echo "===> Adding Python runtime..."                                                         && \
    apk --update add python3 py-pip openssl ca-certificates netcat-openbsd nano                  && \
    apk --update add --virtual build-dependencies python3-dev libffi-dev openssl-dev build-base  && \
    pip install --upgrade pip cffi                                                               && \
    ln -s /usr/bin/python3 /usr/bin/python                                                       && \
    \
    \
    echo "===> Installing Ansible..."  && \
    pip install ansible==2.9.2         && \
    \
    \
    echo "===> Installing handy tools..."  && \
    pip install --upgrade s3cmd pycrypto pywinrm keystoneauth1 python-novaclient python-openstackclient python-neutronclient setuptools requests && \
    apk --update add sshpass openssh-client rsync curl git bash      && \
    \
    echo "===> Instal terraform..."  && \
    wget https://releases.hashicorp.com/terraform/0.12.31/terraform_0.12.31_linux_amd64.zip && \
    unzip terraform_0.12.31_linux_amd64.zip && mv terraform /usr/bin && rm terraform* && \
    \
    echo "===> Removing package list..."  && \
    apk del build-dependencies            && \
    rm -rf /var/cache/apk/*               && \
    \
    \
    echo "===> Adding hosts for convenience..."  && \
    mkdir -p /etc/ansible                        && \
    echo 'localhost' > /etc/ansible/hosts

WORKDIR /source

CMD [ "bash" ]
